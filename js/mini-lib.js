SCREEN = {
    w: 910,
    h: 610
}
GAME_FINISHED = false;
var asset_samus_walk = {
    "sprites": {
        "img/super_metroid_samus.gif": {
            tile: 43,
            tileh: 50,
            map: {
                samus_arriving: [13, 6],
                samus_stand_right: [0, 0],
                samus_stand_left: [0, 1],
                samus_stand_top_right: [0, 2],
                samus_stand_top_left: [0, 3],
                samus_stand_bot_right: [0, 4],
                samus_stand_bot_left: [0, 5],
                samus_kneel_right: [13, 0],
                samus_kneel_left: [13, 1],
                samus_kneel_top_right: [13, 2],
                samus_kneel_top_left: [13, 3],
                samus_kneel_bot_right: [13, 4],
                samus_kneel_bot_left: [13, 5]
            }
        }
    }
};
var asset_astronaut = {
    "sprites": {
        "img/nasa_astronaut.gif": {
            tile: 48,
            tileh: 48,
            map: {
                astronaut_arriving: [0, 0]
            }
        }
    }
};
var asset_gate = {
    "sprites": {
        "img/gate.png": {
            tile: 90,
            tileh: 84,
            map: {
                gate_arriving: [0,0]
            }
        }
    }
}
var Samus = function() {
    this.init = function() {
        this.w = 43;
        this.h = 50;
        this.x = 0;
        this.y = 330;
        this.stop_walking = null;
        this.direction = 0;
        this.jumping = false;
        this.stand_right = [[0, 0]];
        this.stand_left = [[0, 1]];
        this.stand_top_right = [[0, 2]];
        this.stand_top_left = [[0, 3]];
        this.stand_bot_right = [[0, 4]];
        this.stand_bot_left = [[0, 5]];
        this.kneel_right = [[12, 0]];
        this.kneel_left = [[12, 1]];
        this.walking_right = [
            [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0]
        ];
        this.walking_left = [
            [11, 1], [10, 1], [9, 1], [8, 1], [7, 1], [6, 1], [5, 1], [4, 1], [3, 1], [2, 1], [1, 1]
        ];
        this.walking_top_right = [
            [1, 2], [2, 2], [3, 2], [4, 2], [5, 2], [6, 2], [7, 2], [8, 2], [9, 2], [10, 2], [11, 2]
        ];
        this.walking_top_left = [
            [11, 3], [10, 3], [9, 3], [8, 3], [7, 3], [6, 3], [5, 3], [4, 3], [3, 3], [2, 3], [1, 3]
        ];
        this.walking_bot_right = [
            [1, 4], [2, 4], [3, 4], [4, 4], [5, 4], [6, 4], [7, 4], [8, 4], [9, 4], [10, 4], [11, 4]
        ];
        this.walking_bot_left = [
            [11, 5], [10, 5], [9, 5], [8, 5], [7, 5], [6, 5], [5, 5], [4, 5], [3, 5], [2, 5], [1, 5]
        ];
        this.jump_right = [[7, 6], [8, 6], [9, 6]];
        this.jump_left = [[10, 6], [11, 6], [12, 6]];

        this.place = function(x, y) {
            if (x != undefined) this.obj.x = x;
            if (y != undefined) this.obj.y = y;
        }
    }
    this.walk = function(speed) {
        var speed = speed || 1;
        var samus = this.obj;
        function run() {
            samus.x += 1 * speed;
        }
        this.stop_walking = setInterval(run, 5);
    }
    this.stop = function() {
        clearInterval(this.stop_walking);
    }
    this.jump = function() {
        var samus = this.obj;
    }
    this.obj = undefined;
    this.run = function() {
        /* Required to init the Samus representation. */
        this.obj = Crafty.e("2D, Canvas, samus_arriving, SpriteAnimation, Keyboard");
        var samus = this.obj;
        samus.attr({x: this.x, y: this.y, w: this.w, h: this.h});
    }
};
var Astronaut = function() {
    this.init = function() {
        this.w = 43;
        this.h = 50;
        this.x = 0;
        this.y = 330;
        this.saved = false;
        this.obj = undefined;

        this.place = function(x, y) {
            if (x != undefined) this.obj.x = x;
            if (y != undefined) this.obj.y = y;
        }
    }
    this.run = function() {
        this.obj = Crafty.e("2D, Canvas, astronaut_arriving, SpriteAnimation, Keyboard");
        var astronaut = this.obj;
        astronaut.attr({x: this.x, y: this.y, w: this.w, h: this.h});
    }
};
var Gate = function() {
    this.init = function() {
        this.w = 90;
        this.h = 84;
        this.x = 600;
        this.y = 600;
        this.obj = undefined;
        this.opened = false;
        this.open = [
            [0, 0], [0, 1], [0, 2], [0, 3]
        ];

        this.place = function(x, y) {
            if (x != undefined) this.obj.x = x;
            if (y != undefined) this.obj.y = y;
        }
    }
    this.run = function() {
        this.obj = Crafty.e("2D, Canvas, gate_arriving, SpriteAnimation");
        var gate = this.obj;
        gate.attr({x: this.x, y: this.y, w: this.w, h: this.h});
    }
};
Crafty.c("Platform", {
    init: function() {
        this.addComponent("Floor, 2D, Canvas, Color");
        this.w = 100;
        this.h = 10;
        this.color('darkblue');
    },
    remove: function() {},
    place: function(x, y, w) {
        if (w != undefined) this.w = w;
        this.x = x;
        this.y = y;
        return this;
    },
    move_it: function(direction, speed, reach) {
        this.color('darkgreen');
        var init_x = this.x;
        var init_y = this.y;
        var reach = reach || 100;
        var speed = speed || 1;
        var sens = 1;
        if (direction == "up" || direction == "left") {
            reach = -reach;
            sens = -1;
        }
        var platform = this;
        setInterval(function () {
            if (direction == "up" || direction == "down") {
                platform.y += sens;
                samus.obj.y += sens;
                if (platform.y == init_y + reach) {
                    platform.y = init_y;
                }
            } else if (direction == "left" || direction == "right") {
                platform.x += sens;
                samus.obj.x += sens;
                if (platform.x == init_x + reach) {
                    platform.x = init_x;
                }
            }
        }, 100/speed);
        return this;
    }
});
Crafty.c("Wall", {
    init: function() {
        this.addComponent("Floor, 2D, Canvas, Color, Collision");
        this.w = 10;
        this.h = 100;
        this.color('blue');
        this.checkHits('samus_arriving');
        this.onHit('samus_arriving', function(hd) {
            hd[0].obj.trigger("stopWalking");
        });
    },
    remove: function() {},
    place: function(x, y) {
        this.x = x;
        this.y = y;
        return this;
    }
});
Crafty.init(SCREEN.w, SCREEN.h, document.getElementById('game'));
Crafty.c("Prisoner", {
    init: function() {
        this.addComponent("2D, Canvas, astronaut");
    }
});
/* Prisoner */
var prisoner = new Astronaut();
prisoner.init();
Crafty.load(asset_astronaut, prisoner.run);
prisoner.obj = Crafty.e("2D, Canvas, astronaut_arriving, SpriteAnimation, Keyboard, Gravity, Collision");
prisoner.obj.gravity('Floor');
prisoner.obj.attr({x: prisoner.x, y: prisoner.y, w: prisoner.w, h: prisoner.h});
prisoner.obj.onHit('samus_arriving', function(hd) {
    hd[0].obj.trigger("stopWalking");
    if (!prisoner.saved) {
        prisoner.saved = true;
        prisoner.obj.attach(Crafty.e('2D, DOM, Text').attr({
            x: prisoner.obj.x,
            y: prisoner.obj.y-20,
            w: 180
        })
        .textColor('white')
        .text('You saved me! Thank you sooo much!<br/>Now, go to the next room, hurry!'));
    }
});
/* Gate */
var gate = new Gate();
gate.init();
Crafty.load(asset_gate, gate.run);
gate.obj = Crafty.e("2D, Canvas, gate_arriving, SpriteAnimation, Keyboard, Gravity, Collision");
gate.obj.gravity('Floor');
gate.obj.attr({x: gate.x, y: gate.y, w: gate.w, h: gate.h});
gate.obj.reel("gate_open", 1000, gate.open);
gate.obj.onHit('samus_arriving', function(hd) {
    hd[0].obj.trigger("stopWalking");
    if (prisoner.saved && !gate.opened) {
        gate.opened = true;
        gate.obj.animate("gate_open", 1);
        Crafty.scene("winGame", function() {
            Crafty.e('2D, DOM, Text')
                .attr({x: 300, y: (SCREEN.h/2)-50, w:300, h: 200})
                .textColor('white')
                .textFont({weight: 'bold', size: '20px', 'text-align': 'center'})
                .text("You win!<br/>Reload the page to restart...");
        });
        setTimeout(function() {
            Crafty.enterScene("winGame");
            GAME_FINISHED = true;
        }, 1000);
    }
});
/* Samus */
samus = new Samus();
samus.init();
Crafty.load(asset_samus_walk, samus.run);
samus.obj = Crafty.e("2D, Canvas, samus_arriving, SpriteAnimation, Keyboard, Gravity");
samus.obj.gravity('Floor');
samus.obj.gravityConst(1000);
samus.obj.attr({x: samus.x, y: samus.y, w: samus.w, h: samus.h});
samus.obj.reel("stand_right", 1000, samus.stand_right);
samus.obj.reel("stand_left", 1000, samus.stand_left);
samus.obj.reel("stand_top_right", 1000, samus.stand_top_right);
samus.obj.reel("stand_top_left", 1000, samus.stand_top_left);
samus.obj.reel("stand_bot_right", 1000, samus.stand_bot_right);
samus.obj.reel("stand_bot_left", 1000, samus.stand_bot_left);
samus.obj.reel("jump_right", 500, samus.jump_right);
samus.obj.reel("jump_left", 500, samus.jump_left);
samus.obj.reel("walking_right", 1000, samus.walking_right);
samus.obj.reel("walking_left", 1000, samus.walking_left);
samus.obj.reel("walking_top_right", 1000, samus.walking_top_right);
samus.obj.reel("walking_top_left", 1000, samus.walking_top_left);
samus.obj.reel("walking_bot_right", 1000, samus.walking_bot_right);
samus.obj.reel("walking_bot_left", 1000, samus.walking_bot_left);
samus.obj.reel("kneel_right", 1000, samus.kneel_right);
samus.obj.reel("kneel_left", 1000, samus.kneel_left);
samus.obj.bind("stopWalking", function() {
    samus.stop();
    if (samus.direction == -1) {
        samus.obj.x += 1;
    } else {
        samus.obj.x -= 1;
    }
});
samus.obj.bind('KeyDown', function(e) {
    if (e.key == Crafty.keys.RIGHT_ARROW) {
        samus.obj.trigger("stopWalking");
        samus.walk();
        samus.direction = 1;
        samus.obj.animate("walking_right", -1);
    } else if (e.key == Crafty.keys.LEFT_ARROW) {
        samus.obj.trigger("stopWalking");
        samus.walk(-1);
        samus.direction = -1
        samus.obj.animate("walking_left", -1);
    } else if (e.key == Crafty.keys.SPACE) {
        console.log("Jump");
        if (!samus.jumping) {
            samus.jumping = true;
            samus.obj.y -= 100;
            if (samus.direction == -1) {
                samus.obj.animate("jump_left", 1);
                setTimeout(function() {
                    samus.obj.animate("walking_left", -1);
                    samus.jumping = false;
                }, 500);
            } else {
                samus.obj.animate("jump_right", 1);
                if (samus.direction == 1) {
                    setTimeout(function() {
                        samus.obj.animate("walking_right", -1);
                        samus.jumping = false;
                    }, 500);
                } else {
                    setTimeout(function() {
                        samus.obj.animate("stand_right", -1);
                        samus.jumping = false;
                    }, 500);
                }
            }
        }
    } else if (e.key == Crafty.keys.UP_ARROW) {
        if (samus.direction == -1) {
            samus.obj.animate("walking_top_left", -1);
        } else if (samus.direction == 1) {
            samus.obj.animate("walking_top_right", -1);
        } else {
            samus.obj.animate("stand_top_right", -1);
        }
    }
}).bind('KeyUp', function(e) {
    if (e.key == Crafty.keys.RIGHT_ARROW) {
        //samus.animate("walking_right", 0);
        samus.obj.animate("walking_right", 1);
        samus.obj.pauseAnimation();
        samus.obj.animate("stand_right", 1);
        samus.obj.trigger("stopWalking");
        samus.direction = 0;
    } else if (e.key == Crafty.keys.LEFT_ARROW) {
        samus.obj.animate("walking_left", 1);
        samus.obj.pauseAnimation();
        samus.obj.animate("stand_left", 1);
        samus.obj.trigger("stopWalking");
        samus.direction = 0;
    } else if (e.key == Crafty.keys.UP_ARROW) {
        if (samus.direction == -1) {
            samus.obj.animate("walking_left", -1);
        } else if (samus.direction == 1) {
            samus.obj.animate("walking_right", -1);
        } else {
            samus.obj.animate("stand_right", -1);
        }
    }
    //samus.jumping = false;
});
setInterval(function(){
    if (samus.obj.x < 0) samus.obj.x = 0;
    if (samus.obj.x > SCREEN.w-25) samus.obj.x = SCREEN.w-25;
    if (samus.obj.y < -50) samus.obj.y = 0;
    if (samus.obj.y > SCREEN.h+100 && !GAME_FINISHED) {
        console.log("You loose!");
        Crafty.scene("loseGame", function() {
            Crafty.e('2D, DOM, Text')
                .attr({x: 300, y: (SCREEN.h/2)-50, w:300, h: 200})
                .textColor('white')
                .textFont({weight: 'bold', size: '20px', 'text-align': 'center'})
                .text("You lose!<br/>Reload the page to restart...");
        });
        Crafty.enterScene("loseGame");
        GAME_FINISHED = true;
    }
}, 10);
