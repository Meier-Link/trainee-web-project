Trainee Web Project
===================

This is a little and funny project bound to make young trainees discover
webdevelopment and javascript through the creation of a little platform game.

Installation
------------

This is made to be simple and easy to deploy.
Simply git clone the repository, or - for the trainee - unzip the archive of
this repository.

Then, you have just to open the index.html file in the browser.

Dependencies
------------

This project doesn't have external dependencies. However, this project uses:
* crafty.js for all the game engine

Usage
-----

In fact, this project just provide a layer over crafty.js to make the trainee
creating game scenes in few lines of code.

TODO: It'll be developed later, when the project has a real form.
